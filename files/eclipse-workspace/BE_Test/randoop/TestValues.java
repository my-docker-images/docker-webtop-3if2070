import randoop.*;

  public class TestValues {

    @TestValue
    public static int i = 0;

    @TestValue
    public static String s1 = null;

    @TestValue
    public static String s2 = "hi";

    @TestValue
    public static int[] a1 = new int[] { 1, 2, 3 }; 

  }