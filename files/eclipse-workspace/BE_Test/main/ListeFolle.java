import java.util.Arrays;

public class ListeFolle {
	
	
	/* taille max de la liste à ne pas dépasser */
	private /*@ spec_public @*/ int maxSize;
	
	/* la liste est un tableau simple et primitif, 
	 * on n'utilisera pas de structures avancées de type Collection... */
	private /*@ spec_public @*/ int[] tab;
	
	// Ajoutez d'autres attributs si besoin 
	
	// private ...
	
	public enum Side {Left, Right};
	
    /*@ normal_behavior
	  @ 	requires _maxSize > 0;
	  @ 	ensures  maxSize == _maxSize && tab.length == 0;
      @ also 
	  @ exceptional_behavior
	  @ 	requires _maxSize <= 0; 
  	  @		signals (Exception e) true;
	  @*/
	public ListeFolle(int _maxSize) throws Exception {
		if (_maxSize <= 0) {
			throw new Exception("Invalid _maxSize argument: must be > 0");
		}
		this.maxSize = _maxSize;
		this.tab = new int[0];
	}
	
    /*@ normal_behavior
	  @     requires _maxSize > 0 && _tab.length <= _maxSize && (\forall int i; 0 <= i && i < _tab.length; _tab[i] > 0);
	  @ 	ensures maxSize == _maxSize && _tab.length == tab.length;
	  @ also 
	  @ exceptional_behavior
	  @ 	requires _maxSize <= 0 || _tab.length > _maxSize;
	  @		signals (Exception e) true;
	  @*/
	public ListeFolle(int _maxSize, /*@ non_null @*/ int[] _tab) throws Exception {}	
	
	public ListeFolle(/*@ non_null @*/ ListeFolle list){}

	public /*@ pure @*/ int getCurrentSize(){return 0;}
	
	public /*@ pure @*/ int getMaxSize(){return 0;}	
	
	public /*@ pure @*/ int[] getTab() {return new int[0];}
	
	public void insert(int cell, int position) throws Exception {} 			/* attention : pensez à ne pas dépasser la taille max */
	
	public /*@ pure @*/ boolean contains(int cell) {return false;}

	public void split(int position) throws Exception {} 					/* attention : pensez à ne pas dépasser la taille max */

	public void mix(int position, Side side) throws Exception {} 			/* attention : verifiez que cela est possible */  
	
	public void destroy(int position) throws Exception {} 					/* attention : verifiez que la cellule est bien entourée  */
	
	public void swap(int position_1, int position_2) throws Exception {}
	
	@Override
	public boolean equals(Object obj) { return false; }  					/* à ne pas oublier */
	
	
	@Override
	public String toString() {
		return "| " + this.maxSize + " - " + Arrays.toString(this.tab) + " |";
	}

}
