
# docker-webtop-3if2070

## Why

This image, based on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop), 
provides the environment needed for the 3if2070 course of CentraleSupélec.

It uses the [Xfce](https://www.xfce.org) environment.

## Details

- See details on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop/-/tree/xfce)
- if docker is installed on your computer, you can run (amd64 architecture only) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-webtop-3if2070:latest`

