FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-webtop:xfce

RUN \
  echo "**** install runtime dependencies ****" && \
  apt-get update && \
  apt-get install -y \
    openjdk-8-source \
    openjdk-11-jre

RUN \
  cd /tmp && \
  curl -O -L https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/2020-09/R/eclipse-java-2020-09-R-linux-gtk-x86_64.tar.gz && \
  cd /opt && \
  tar zxf /tmp/eclipse-java-2020-09-R-linux-gtk-x86_64.tar.gz

RUN \
  echo "**** clean up ****" && \
  apt-get clean && \
  rm -rf \
    /tmp/* \
    /var/lib/apt/lists/* \
    /var/tmp/*

COPY files /init-config

